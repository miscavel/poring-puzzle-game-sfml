# Poring Puzzle Game SFML

Poring Puzzle Game made in SFML-2.3.2

Project compiled using Visual Studio 2010 32-bit

The game is inspired by the Collapse Game mechanics.

Gameplay :
1. Click on a tile that is linked with 2 or more other tiles of the same color to destroy them.
2. Unlike in Collapse, the tiles do not automatically converge depending on the stage's gravity.\
   If the gravity is OFF, press RMB to converge the tiles.\
   If the gravity is ON, the tiles automatically converge.
3. Clear all the tiles on the stage to proceed to the next level.
4. If the stage becomes impossible to clear, press the 'R' key to reset the stage.
4. Clear 10 stages within 5 minutes to win the game.
5. The stages can be edited via mapcomp.txt.


![Gameplay Video](Media/trailer.mp4)

Credits to Gravity Co., Ltd for the Poring Sprites.