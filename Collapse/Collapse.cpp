#include <iostream>
#include <SFML/Graphics.hpp>
#include <vector>
#include <time.h>
#include <Windows.h>
#include <math.h>
#include <sstream>
#include <fstream>
#include <conio.h>

class Block
{
	bool hit, dead;
	float sizex,sizey;
	int phase,anidelay,animdelay;
	int textureSizeX, textureSizeY;
	int phasen,phaseh,phased;
public :
	sf::RectangleShape block_shape;
	Block(float sizexv = 0, float sizeyv = 0)
	{
		dead = false;
		hit = false;
		phase = rand()%4;
		phasen = 3;
		phaseh = 5;
		phased = 4;
		sizex = sizexv;
		sizey = sizeyv;
		anidelay = 12;
		animdelay = 12;
		block_shape.setSize(sf::Vector2f(sizex,sizey));
	}
	
	void setImage(sf::Texture &Texture)
	{
		block_shape.setTexture(&Texture);
		if(dead == true)
		{
			textureSizeX = 64;
		}
		else if(hit == true)
		{
			textureSizeX = (Texture.getSize().x/5);
		}
		else
		{
			textureSizeX = (Texture.getSize().x/4);
		}

		if(dead == true)
		{
			textureSizeY = 52;
			block_shape.setTextureRect(sf::IntRect(167 + 386 * phase, 145, textureSizeX, textureSizeY));
		}
		else
		{
			textureSizeY = Texture.getSize().y;
			block_shape.setTextureRect(sf::IntRect(phase * textureSizeX, 0, textureSizeX, textureSizeY));
		}
		
	}

	void setCoordinate(float x, float y)
	{
		block_shape.setPosition(x,y);
	}

	float getPositionY()
	{
		return block_shape.getPosition().y;
	}

	float getPositionX()
	{
		return block_shape.getPosition().x;
	}

	void setColour(sf::Color Color)
	{
		block_shape.setFillColor(Color);
	}

	void animateImage()
	{
		if(dead == true)
		{
			if(anidelay > 0)
			{
				anidelay = anidelay - 12;
			}
			else
			{
				anidelay = animdelay;
				if(phase < phased)
				{
					phase++;
				}
				else
				{
					phase = 0;
				}
			}
			block_shape.setTextureRect(sf::IntRect(167 + 386 * phase, 145, textureSizeX, textureSizeY));
		}
		else
		{
			if(anidelay > 0)
			{
				if(hit == true)
				{
					anidelay = anidelay - 4;
				}
				else
				{
					anidelay = anidelay - 3;
				}
			}
			else
			{
				anidelay = animdelay;
				if(hit == false)
				{
					if(phase < phasen)
					{
						phase++;
					}
					else
					{
						phase = 0;
					}
				}
				else
				{
					if(phase < phaseh)
					{
						phase++;
					}
					else
					{
						phase = 0;
					}
				}
			}
			block_shape.setTextureRect(sf::IntRect(phase * textureSizeX, 0, textureSizeX, textureSizeY));
		}
	}

	int getAnimDelay()
	{
		return animdelay;
	}

	int getPhaseH()
	{
		return phaseh;
	}
	
	void hitCheck(bool hit_check)
	{
		hit = hit_check;
	}

	bool getHit()
	{
		return hit;
	}

	void deadCheck(bool dead_check)
	{
		dead = dead_check;
	}

	bool getDead()
	{
		return dead;
	}

	void setPhase(int phasev)
	{
		phase = phasev;
	}

	sf::RectangleShape getBlock()
	{
		return block_shape;
	}
};

void loadScreen(sf::RenderWindow &Window,std::vector<Block> &BlockBar,sf::Text Score_Text,sf::Text Stage_Text,sf::Text Time_Text,sf::Text GameOver_Text,bool pause)
{
	Window.draw(GameOver_Text);
	Window.draw(Time_Text);
	Window.draw(Score_Text);
	Window.draw(Stage_Text);
	for(int i = 0; i < BlockBar.size(); i++)
	{
		BlockBar[i].animateImage();
		Window.draw(BlockBar[i].getBlock());
	}
	Window.display();
	Window.clear();
}

void updateScreen(std::vector<Block> &BlockBar,std::vector<std::vector<int>> FullBar,int ColSize,int RowSize,sf::Texture &poringTexture,sf::Texture &poporingTexture,sf::Texture &pouringTexture,sf::Texture &poringHitTexture,sf::Texture &poporingHitTexture,sf::Texture &pouringHitTexture, int temp,sf::Texture &poringDeadTexture,sf::Texture &poporingDeadTexture, sf::Texture &pouringDeadTexture)
{
	for(int i = 0; i < BlockBar.size(); i++)
	{
		std::cout << FullBar[int(BlockBar[i].getPositionY()/55)-ColSize+FullBar.size()][int(BlockBar[i].getPositionX()/57)] << "\n";
		if(FullBar[int(BlockBar[i].getPositionY()/55)-ColSize+FullBar.size()][int(BlockBar[i].getPositionX()/57)] == 0)
		{
			BlockBar[i].setPhase(0);
			BlockBar[i].deadCheck(true);
			if(temp == 1)
			{
				BlockBar[i].setImage(poringDeadTexture);
			}
			else if(temp == 2)
			{
				BlockBar[i].setImage(poporingDeadTexture);
			}
			else if(temp == 3)
			{
				BlockBar[i].setImage(pouringDeadTexture);
			}
		}
		else if(FullBar[int(BlockBar[i].getPositionY()/55)-ColSize+FullBar.size()][int(BlockBar[i].getPositionX()/57)] == -1)
		{
			BlockBar[i].setPhase(0);
			BlockBar[i].hitCheck(true);
			BlockBar[i].setImage(poringHitTexture);
		}
		else if(FullBar[int(BlockBar[i].getPositionY()/55)-ColSize+FullBar.size()][int(BlockBar[i].getPositionX()/57)] == -2)
		{
			BlockBar[i].setPhase(0);
			BlockBar[i].hitCheck(true);
			BlockBar[i].setImage(poporingHitTexture);
		}
		else if(FullBar[int(BlockBar[i].getPositionY()/55)-ColSize+FullBar.size()][int(BlockBar[i].getPositionX()/57)] == -3)
		{
			BlockBar[i].setPhase(0);
			BlockBar[i].hitCheck(true);
			BlockBar[i].setImage(pouringHitTexture);
		}
		else
		{
			if(BlockBar[i].getHit() == true)
			{
				BlockBar[i].setPhase(0);
				BlockBar[i].hitCheck(false);
				if(FullBar[int(BlockBar[i].getPositionY()/55)-ColSize+FullBar.size()][int(BlockBar[i].getPositionX()/57)] == 1)
				{
					BlockBar[i].setImage(poringTexture);
				}
				else if(FullBar[int(BlockBar[i].getPositionY()/55)-ColSize+FullBar.size()][int(BlockBar[i].getPositionX()/57)] == 2)
				{
					BlockBar[i].setImage(poporingTexture);
				}
				else if(FullBar[int(BlockBar[i].getPositionY()/55)-ColSize+FullBar.size()][int(BlockBar[i].getPositionX()/57)] == 3)
				{
					BlockBar[i].setImage(pouringTexture);
				}
			}
		}
	}
}

void generateScreen(std::vector<Block> &BlockBar,std::vector<std::vector<int>> FullBar,int ColSize,int RowSize,sf::Texture &poringTexture,sf::Texture &poporingTexture,sf::Texture &pouringTexture)
{
	BlockBar.clear();
	for(int i = FullBar.size()-1, k = 1; i >= 0; i--, k++)
	{
		for(int j = 0; j < ColSize; j++)
		{
			Block BlockSingle(57,55);
			if(FullBar[i][j] == 0)
			{
				BlockSingle.setColour(sf::Color(0,0,0,0));
			}
			else if(FullBar[i][j] == 1)
			{
				BlockSingle.setImage(poringTexture);
			}
			else if(FullBar[i][j] == 2)
			{
				BlockSingle.setImage(poporingTexture);
			}
			else if(FullBar[i][j] == 3)
			{
				BlockSingle.setImage(pouringTexture);
			}
			else if(FullBar[i][j] == 4)
			{
				BlockSingle.setColour(sf::Color(255,153,153,255));
			}
			BlockSingle.setCoordinate(j * 57, (RowSize-k) * 55);
			BlockBar.push_back(BlockSingle);
		}
	}
}

void deleteRow(std::vector<std::vector<int>> &FullBar, int ColSize)
{
	while(true)
	{
		int empty_check = 0;
		for(int j = 0; j < ColSize; j++)
		{
			if(FullBar[0][j] == 0)
			{
				empty_check++;
			}
		}
		if(empty_check == ColSize)
		{
			for(int i = 0; i < FullBar.size()-1; i++)
			{
				for(int j = 0; j < ColSize; j++)
				{
					FullBar[i][j] = FullBar[i+1][j];
				}
			}
			FullBar.pop_back();
			if(FullBar.size() == 0)
			{
				break;
			}
		}
		else
		{
			break;
		}
	}
}

void blockAdjust(std::vector<std::vector<int>> &FullBar, int ColSize, int RowSize)
{
	//VerticalAdjust
	if(FullBar.size() > 0)
	{	
		for(int i = FullBar.size()-1; i > 0; i--)
		{
			for(int j = 0; j < ColSize; j++)
			{
				for(int k = i-1; FullBar[i][j] == 0 && k >= 0; k--)
				{
					FullBar[i][j] = FullBar[k][j];
					FullBar[k][j] = 0;
				}
			}
		}
	
		//HorizontalAdjust1
		for(int j = ceil(double(double(ColSize)/2))-1; j > 0; j--)
		{
			for(int k = j-1; FullBar[FullBar.size()-1][j] == 0 && k >= 0; k--)
			{
				for(int i = 0; i < FullBar.size(); i++)
				{
					FullBar[i][j] = FullBar[i][k];
					FullBar[i][k] = 0;
				}
			}
		}

		//HorizontalAdjust2
		for(int j = ceil(double(double(ColSize)/2)); j < ColSize-1; j++)
		{
			for(int k = j+1; FullBar[FullBar.size()-1][j] == 0 && k < ColSize; k++)
			{
				for(int i = 0; i < FullBar.size(); i++)
				{
					FullBar[i][j] = FullBar[i][k];
					FullBar[i][k] = 0;
				}
			}
		}
	}
}

bool checkTile(std::vector<std::vector<int>> &FullBar, int ColSize, int RowSize, int positionY, int positionX, int &score, sf::Text Score_Text, sf::Text Stage_Text, std::vector<Block> &BlockBar, sf::Texture &poringTexture, sf::Texture &poporingTexture, sf::Texture &pouringTexture, sf::Texture &poringHitTexture, sf::Texture &poporingHitTexture, sf::Texture &pouringHitTexture, sf::Texture &poringDeadTexture, sf::Texture &poporingDeadTexture, sf::Texture &pouringDeadTexture, sf::RenderWindow &Window, int temp, sf::Text Time_Text, bool pause, sf::Text GameOver_Text)
{
	std::vector<int> PositionY_Bank;
	std::vector<int> PositionX_Bank;
	
	int core_tile = FullBar[positionY][positionX], tileMatch = 1;

	if(core_tile != 0)
	{
		FullBar[positionY][positionX] = FullBar[positionY][positionX] * (-1);
		PositionY_Bank.push_back(positionY);
		PositionX_Bank.push_back(positionX);
	
		while(PositionX_Bank.size() != 0 && PositionY_Bank.size() != 0)
		{
			int indexy, indexx;
			indexy = PositionY_Bank[PositionY_Bank.size()-1];
			indexx = PositionX_Bank[PositionX_Bank.size()-1];
			PositionY_Bank.pop_back();
			PositionX_Bank.pop_back();
			if(indexy - 1 >= 0)
			{
				if(FullBar[indexy - 1][indexx] == core_tile)
				{
					tileMatch++;
					FullBar[indexy - 1][indexx] = FullBar[indexy - 1][indexx] * (-1);
					PositionY_Bank.push_back(indexy - 1);
					PositionX_Bank.push_back(indexx);
				}
			}
			if(indexy + 1 < FullBar.size())
			{
				if(FullBar[indexy + 1][indexx] == core_tile)
				{
					tileMatch++;
					FullBar[indexy + 1][indexx] = FullBar[indexy + 1][indexx] * (-1);
					PositionY_Bank.push_back(indexy + 1);
					PositionX_Bank.push_back(indexx);
				}
			}
			if(indexx - 1 >= 0)
			{
				if(FullBar[indexy][indexx - 1] == core_tile)
				{
					tileMatch++;
					FullBar[indexy][indexx - 1] = FullBar[indexy][indexx - 1] * (-1);
					PositionY_Bank.push_back(indexy);
					PositionX_Bank.push_back(indexx - 1);
				}
			}
			if(indexx + 1 < ColSize)
			{
				if(FullBar[indexy][indexx + 1] == core_tile)
				{
					tileMatch++;
					FullBar[indexy][indexx + 1] = FullBar[indexy][indexx + 1] * (-1);
					PositionY_Bank.push_back(indexy);
					PositionX_Bank.push_back(indexx + 1);
				}
			}
		}

		updateScreen(BlockBar,FullBar,ColSize,RowSize,poringTexture,poporingTexture,pouringTexture,poringHitTexture,poporingHitTexture,pouringHitTexture,temp,poringDeadTexture,poporingDeadTexture,pouringDeadTexture);
		for(int i = 0; i < 12; i++)
		{
			loadScreen(Window,BlockBar,Score_Text,Stage_Text,Time_Text,GameOver_Text,pause);
		}
		
		if(pause == true)
		{
			return false;
		}

		if(tileMatch >= 3)
		{
			score = score + 10 + (tileMatch - 3) * 10;
			return true;
		}
	}
	return false;
}

int collapseTile(std::vector<std::vector<int>> &FullBar, int ColSize, int RowSize, int mouseY, int mouseX, int &score, sf::Text Score_Text, sf::Text Stage_Text,std::vector<Block> &BlockBar, sf::Texture &poringTexture, sf::Texture &poporingTexture, sf::Texture &pouringTexture, sf::Texture &poringHitTexture, sf::Texture &poporingHitTexture, sf::Texture &pouringHitTexture, sf::Texture &poringDeadTexture, sf::Texture &poporingDeadTexture, sf::Texture &pouringDeadTexture, sf::RenderWindow &Window, sf::Text Time_Text, bool pause, sf::Text GameOver_Text)
{
	bool collapse;
	int temp;
	
	if(FullBar.size() > 0)
	{
		if((ceil(double(mouseY)/55)-1) < (RowSize) && (ceil(double(mouseX)/57)-1) < (ColSize) && (ceil(double(mouseY)/55)-1) >= (RowSize - FullBar.size()))
		{
			temp = FullBar[(ceil(double(mouseY)/55)-1) - (RowSize - FullBar.size())][ceil(double(mouseX)/57)-1];
			collapse = checkTile(FullBar,ColSize,RowSize,(ceil(double(mouseY)/55)-1) - (RowSize - FullBar.size()),ceil(double(mouseX)/57)-1,score,Score_Text,Stage_Text,BlockBar,poringTexture,poporingTexture,pouringTexture,poringHitTexture,poporingHitTexture,pouringHitTexture,poringDeadTexture,poporingDeadTexture,pouringDeadTexture,Window,temp,Time_Text,pause,GameOver_Text);
			for(int i = 0; i < FullBar.size(); i++)
			{
				for(int j = 0; j < ColSize; j++)
				{
					if(FullBar[i][j] < 0)
					{
						if(collapse == true)
						{
							FullBar[i][j] = 0;
						}
						else
						{
							FullBar[i][j] = temp;
						}
					}
				}
			}
		}
	}
	
	if(collapse == true)
	{
		return temp;
	}
	return 0;
}

void main()
{
	int gravity;
	FreeConsole();
	bool destroy = true;
	bool generate = true;
	int color_amount = 3;
	int block_amount = 0;
	int time_limit = 300, minute, second;
	int stage = 1;
	int score = 0;
	int stage_number = 0;
	bool stage_load = true;
	srand(time(NULL));
	bool pause = false;
	int ColSize = 11;
	int RowSize = 11;
	std::vector<int> ColBar;
	std::vector<std::vector<int>> FullBar;
	std::vector<Block> BlockBar;
	
	sf::Sprite Sprite;

	sf::RectangleShape Background(sf::Vector2f(1366,768));

	sf::Texture backgroundTexture;
	sf::Texture poringTexture;
	sf::Texture poporingTexture;
	sf::Texture pouringTexture;
	sf::Texture poringHitTexture;
	sf::Texture poporingHitTexture;
	sf::Texture pouringHitTexture;
	sf::Texture poringDeadTexture;
	sf::Texture poporingDeadTexture;
	sf::Texture pouringDeadTexture;
	poringHitTexture.loadFromFile("PORINGATTACKED.png");
	poringTexture.loadFromFile("PORING.png");
	poporingHitTexture.loadFromFile("POPORINGATTACKED.png");
	poporingTexture.loadFromFile("POPORING.png");
	pouringHitTexture.loadFromFile("POURINGATTACKED.png");
	pouringTexture.loadFromFile("POURING.png");
	
	poringDeadTexture.loadFromFile("PORINGDED.png");
	poporingDeadTexture.loadFromFile("POPORINGDED.png");
	pouringDeadTexture.loadFromFile("POURINGDED.png");

	backgroundTexture.loadFromFile("japan.jpg");
	Background.setTexture(&backgroundTexture);

	sf::Clock Clock;
	sf::Time Time;
	sf::Font NotifFont;
	sf::Text GameOver_Text, Score_Text, Stage_Text, Time_Text;
	NotifFont.loadFromFile("Lumberjack.otf");
	GameOver_Text.setFont(NotifFont);
	GameOver_Text.setCharacterSize(32);
	GameOver_Text.setColor(sf::Color(255,255,255,255));
	GameOver_Text.setPosition(680, 192);
	GameOver_Text.setString("Game Over");

	Score_Text.setFont(NotifFont);
	Score_Text.setCharacterSize(32);
	Score_Text.setColor(sf::Color(255,255,255,255));
	Score_Text.setPosition(-100, 0);
	
	Stage_Text.setFont(NotifFont);
	Stage_Text.setCharacterSize(32);
	Stage_Text.setColor(sf::Color(255,255,255,255));
	Stage_Text.setPosition(680, 64);

	Time_Text.setFont(NotifFont);
	Time_Text.setCharacterSize(32);
	Time_Text.setColor(sf::Color(255,255,255,255));
	Time_Text.setPosition(680,0);
	
	std::ostringstream convert_minute, convert_second;
	minute = time_limit / 60;
	second = (time_limit - minute * 60);
	convert_minute << minute;
	convert_second << second;

	if(second >= 10)
	{
		Time_Text.setString("Time : " + convert_minute.str() + ":" + convert_second.str());
	}
	else
	{
		Time_Text.setString("Time : " + convert_minute.str() + ":0" + convert_second.str());
	}

	sf::Time BlockSpawn;
	sf::Clock BlockSpawnClock;

	sf::Vector2i screenDimension(1366,768);
	sf::RenderWindow Window(sf::VideoMode(screenDimension.x, screenDimension.y),"Poring Squash",sf::Style::Fullscreen);
	
	Window.setFramerateLimit(60);
	Window.setTitle("Collapse");
	while(Window.isOpen())
	{
		std::ostringstream convert_score, convert_stage;

		BlockSpawn = BlockSpawnClock.getElapsedTime();
		sf::Event Event;
		if(stage_load == true)
		{
			int stage_scan;
			int block_value;
			std::string trash;
			std::ifstream openmap("mapcomp.txt");
			FullBar.clear();
			if(openmap.is_open())
			{
				openmap >> stage_scan;
				openmap >> gravity;
				while(stage_scan != stage_number)
				{
					for(int i = 0; i < RowSize + 1; i++)
					{
						std::getline(openmap,trash);
					}
					openmap >> stage_scan;
					openmap >> gravity;
				}
				for(int i = 0; i < RowSize; i++)
				{
					for(int j = 0; j < ColSize; j++)
					{
						openmap >> block_value;
						ColBar.push_back(block_value);
					}
					FullBar.push_back(ColBar);
					ColBar.clear();
				}
			}
			generate = true;
			stage_load = false;
		}
		while(Window.pollEvent(Event))
		{
			if(Event.type == sf::Event::Closed)
			{
				Window.close();
			}
			if(Event.type == sf::Event::Resized)
			{
				Window.setView(sf::View(sf::FloatRect(0, 0, Event.size.width, Event.size.height)));
			}
			if(Event.type == sf::Event::MouseButtonPressed)
			{
				if(destroy == false)
				{
					if(Event.mouseButton.button == sf::Mouse::Left)
					{
						int tiletemp = collapseTile(FullBar,ColSize,RowSize,Event.mouseButton.y,Event.mouseButton.x,score,Score_Text,Stage_Text,BlockBar,poringTexture,poporingTexture,pouringTexture,poringHitTexture,poporingHitTexture,pouringHitTexture,poringDeadTexture,poporingDeadTexture,pouringDeadTexture,Window,Time_Text,pause,GameOver_Text);
						if(tiletemp > 0)
						{
							generate = true;
							destroy = true;
						}
						updateScreen(BlockBar,FullBar,ColSize,RowSize,poringTexture,poporingTexture,pouringTexture,poringHitTexture,poporingHitTexture,pouringHitTexture,tiletemp,poringDeadTexture,poporingDeadTexture,pouringDeadTexture);
					}
					else if(Event.mouseButton.button == sf::Mouse::Right)
					{
						if(pause == false && gravity == 0)
						{
							blockAdjust(FullBar,ColSize,RowSize);
							generate = true;
						}
					}
				}
			}
			/*if(Event.type == sf::Event::MouseButtonReleased)
			{
				if(Event.mouseButton.button == sf::Mouse::Right)
				{
					
				}
			}*/
			if(Event.type == sf::Event::KeyReleased)
			{
				if(Event.key.code == sf::Keyboard::Escape)
				{
					Window.close();
				}
				else if(Event.key.code == sf::Keyboard::R)
				{
					stage_load = true;
					generate = true;
				}
			}
		}
		if(pause == false)
		{
			convert_stage << stage;
			Stage_Text.setString("Stage : "+convert_stage.str());
		}

		if(gravity == 1)
		{
			blockAdjust(FullBar,ColSize,RowSize);
		}

		Time = Clock.getElapsedTime();
		if(pause == false && stage != 1)
		{
			if(Time.asSeconds() >= 1)
			{
				Clock.restart();
				convert_minute.str("");
				convert_second.str("");
				time_limit--;
				minute = time_limit / 60;
				second = (time_limit - minute * 60);
				convert_minute << minute;
				convert_second << second;
				if(second >= 10)
				{
					Time_Text.setString("Time : " + convert_minute.str() + ":" + convert_second.str());
				}
				else
				{
					Time_Text.setString("Time : " + convert_minute.str() + ":0" + convert_second.str());
				}
			}
		}

		if(FullBar.size() > 0)
		{
			deleteRow(FullBar,ColSize);
		}
		if(generate == true)
		{
			if(destroy == true)
			{
				for(int i = 0; i < 8; i++)
				{
					loadScreen(Window,BlockBar,Score_Text,Stage_Text,Time_Text,GameOver_Text,pause);
				}
			}
			generateScreen(BlockBar, FullBar, ColSize, RowSize,poringTexture,poporingTexture,pouringTexture);
			generate = false;
			destroy = false;
		}

		if(minute <= 0 && second <= 0)
		{
			GameOver_Text.setString("Game Over!");
			pause = true;
		}
		else
		{
			if(pause == false)
			{
				if(gravity == 0)
				{
					GameOver_Text.setString("Gravity : Off");
				}
				else
				{
					GameOver_Text.setString("Gravity : On");
				}
			}
			else
			{
				GameOver_Text.setString("Congratulations!");
			}
		}
		loadScreen(Window,BlockBar,Score_Text,Stage_Text,Time_Text,GameOver_Text,pause);

		if(FullBar.size() == 0)
		{
			if(stage >= 9)
			{
				stage_load = true;
				stage_number = 99;
				pause = true;
				Stage_Text.setString("Completed!");
			}
			else
			{
				stage++;
				stage_number++;
				stage_load = true;
			}
		}
	}
}

//Generate Blocks;

/*if(BlockSpawn.asSeconds() > Speed)
			{
				int block;
				block_amount++;
				block = rand()%(color_amount)+1;
				ColBar.push_back(block);
				BlockSpawnClock.restart();
			}*/

/*for(int i = 0; i < ColBar.size(); i++)
			{
				Block.setPosition(i * 32, RowSize * 32);
				if(ColBar[i] == 1)
				{
					Block.setFillColor(sf::Color(153,76,0,100));
				}
				else if(ColBar[i] == 2)
				{
					Block.setFillColor(sf::Color(255,128,0,100));
				}
				else if(ColBar[i] == 3)
				{
					Block.setFillColor(sf::Color(0,255,128,100));
				}
				else if(ColBar[i] == 4)
				{
					Block.setFillColor(sf::Color(255,153,153,100));
				}
				Window.draw(Block);
			}*/

/*if(block_amount == ColSize * 20)
			{
				block_amount = 0;
				stage++;
				if(Speed <= 0.08)
				{
					if(color_amount < 4)
					{
						color_amount++;
						Speed = 0.14;
					}
				}
				else
				{
					Speed = Speed - 0.02;
				}
			}*/
//
//if(FullBar.size() > RowSize)
//			{
//				pause = true;
//				Window.draw(GameOver_Text);
//				Window.display();
//			}